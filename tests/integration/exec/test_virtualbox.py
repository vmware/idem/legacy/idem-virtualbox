def test_create(hub, instance_name, image):
    ret = hub.exec.vbox.create(instance_name, image)
    assert ret == {}


def test_exists(hub, instance_name):
    ret = hub.exec.vbox.exists(instance_name)
    assert ret is True


def test_list(hub, instance_name):
    ret = hub.exec.vbox.list()
    assert instance_name in ret


def test_get(hub, instance_name):
    ret = hub.exec.vbox.get(instance_name)
    assert ret == {}


def test_start(hub, instance_name):
    ret = hub.exec.vbox.start(instance_name)
    assert ret == {}


def test_stop(hub, instance_name):
    ret = hub.exec.vbox.stop(instance_name)
    assert ret == {}


def test_delete(hub, instance_name):
    ret = hub.exec.vbox.delete(instance_name)
    assert ret == {}
